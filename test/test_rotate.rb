require 'minitest/autorun'

require 'board'

class TestRotate < Minitest::Test

	def setup
		@b = Board.new(["OXX","OO ","  X"])
	end
	
	def test_parse_encode
		assert_equal @b.rotate, [[0,-1,-1], [0,-1,1], [1,0,1]]
	end
	
	def test_main_diagonal
		assert_equal @b.main_diagonal, [-1,-1,1]
	end
	
	def test_secondary_diagoal
		assert_equal @b.secondary_diagonal, [1,-1,0]
	end
	
end