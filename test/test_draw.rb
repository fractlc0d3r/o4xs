require 'minitest/autorun'

require 'board'

class TestDraw < Minitest::Test
	def setup
		@b = Board.new ["XOX","XOX","OXO"]
		@b2 = Board.new ["O X","XXO","O  "]
		@b3 = Board.new ["XOX","XOX","OX "]
		@b4 = Board.new ["OOX","XXO","OX "]
	end
	
	def test_draw
		assert_equal @b.check_draw, true
	end
	
	def test_not_draw
		assert_equal @b2.check_draw, false
	end
	
	def test_not_draw2
		assert_equal @b3.check_draw, false
	end
	
	def test_draw_2
		assert_equal @b4.check_draw, true
	end
	
end