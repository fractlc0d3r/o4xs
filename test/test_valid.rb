require 'minitest/autorun'

require 'board'

class TestValid < Minitest::Test
	
	def setup
		@b = Board.new ["XXX","OO ","O X"]
		@b2 = Board.new ["XXX","OOO","   "]
		@b3 = Board.new ["XOO","OOX","   "]
		@b4 = Board.new ["XXO", "OXX","   "]
		@b5 = Board.new ["XO "," OX","XO "]
		@b6 = Board.new ["XOX","XO ","OXO"]
		@b7 = Board.new ["XOX","XOX","OXO"]
	end
	
	def test_x_win_valid
		assert_equal @b.check_validity, true
	end
		
	def test_x_and_o_win
		assert_equal @b2.check_validity, false
	end
	
	def test_o_more_moves_than_x
		assert_equal @b3.check_validity, false
	end
	
	def test_x_too_many_moves
		assert_equal @b4.check_validity, false
	end
	
	def test_o_wins_valid
		assert_equal @b5.check_validity, true
	end
	
	def test_valid_draw_equal_nmoves
		assert_equal @b6.check_validity, true
	end
	
	def test_valid_draw_not_equal_nmoves
		assert_equal @b7.check_validity, true
	end
	
end