require 'minitest/autorun'

require 'board'

class TestWin < Minitest::Test

	def setup
		@b = Board.new ["XXX","OO ","O X"]
		@b2 = Board.new ["X  ","X  ","X  "]
		@b3 = Board.new ["X  "," X ","  X"]
		@b4 = Board.new ["  X"," X ","X  "]
		@b5 = Board.new ["X  "," OX","OX "]
		@b6 = Board.new ["OOO","   ","   "]
	end
	
	def test_row_x_win
		assert_equal @b.check_win('X'), true
	end
	
	def test_col_x_win
		assert_equal @b2.check_win('X'), true
	end
	
	def test_diag_x_win
		assert_equal @b3.check_win('X'), true
		assert_equal @b4.check_win('X'), true
	end
	
	def test_no_win
		assert_equal @b5.check_win('X'), false
		assert_equal @b5.check_win('O'), false
	end
	
	def test_o_win
		assert_equal @b6.check_win('O'), true
	end
	
end