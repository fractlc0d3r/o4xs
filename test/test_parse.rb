require 'minitest/autorun'

require 'board'

class TestParse < Minitest::Test
	
	def setup
		@b = Board.new
	end
	
	def test_board_created
		assert_instance_of Board, @b
	end
	
	def test_parse_nil
		e = assert_raises(Exception) { @b.parse nil }
		assert_equal( "Expecting array", e.message )
	end
	
	def test_parse_non_array
		e = assert_raises(Exception) { @b.parse "a string" }
		assert_equal( "Expecting array", e.message )
	end
	
	def test_parse_input_too_short
		e = assert_raises(Exception) { @b.parse ["OXO","XOX"] }
		assert_equal( "Must enter three lines", e.message )
	end
	
	def test_parse_input_too_long
		e = assert_raises(Exception) { @b.parse ["OXO","XOX","OXO","XOX"] }
		assert_equal( "Must enter three lines", e.message )
	end
	
	def test_parse_three_chars_per_line
		e = assert_raises(Exception) { @b.parse ["OX","XOX","OXO"] }
		assert_equal( "Each line must have 3 characters", e.message )
	end
	
	def test_parse_only_XOspace_allowed
		e = assert_raises(Exception) { @b.parse ["OXY","XOX","OXO"] }
		assert_equal( "Only characters X, O and whitespace are allowed", e.message )
	end
	
	def test_parse_encode
		assert_equal @b.parse(["OXX","OO ","  X"]), [[-1,1,1],[-1,-1,0],[0,0,1]]
	end
end