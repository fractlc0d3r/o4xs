# oxs

## Overview

**oxs** is a TicTacToe Board calculator. It takes board state from standard input, for example:

	XX 
	OOO
 	X

evaluates the game state, and outputs one of the following:

	X Wins!	O Wins!	Draw	Game Incomplete	Invalid Game	Invalid Input
	
## Requirements
The only requirement is [Ruby](https://www.ruby-lang.org/ "Ruby home page") version 2.0.0 or later. If you are using [Bundler](http://bundler.io) with [RVM](http://rvm.io), version 2.2.1 is specified in the *Gemfile* and will be automatically selected.
## Installation
### Clone	 git clone https://bitbucket.org/fractlc0d3r/o4xs.git
		 cd o4xs
 
### Run unit tests

run *rake* or *rake test* to run unit tests

	$ rake test
	/Users/pauls/.rvm/rubies/ruby-2.2.1/bin/ruby -I"lib"  "/Users/pauls/.rvm/rubies/ruby-2.2.1/lib/ruby/2.2.0/rake/rake_test_loader.rb" "test/test_draw.rb" "test/test_parse.rb" "test/test_rotate.rb" "test/test_valid.rb" "test/test_win.rb" 
	Run options: --seed 41349
	
	# Running:
	
	...........................
	
	Finished in 0.004126s, 6543.3828 runs/s, 8482.1629 assertions/s.
	
	27 runs, 35 assertions, 0 failures, 0 errors, 0 skips
	$ 

	## Usage
### Getting help
run **o4xs** with -h or --help command option for help:
	$ ./oxs -h
	Usage: ./oxs.rb [options]
	./oxs.rb is a TicTacToe board calculator
	that takes board state from STDIN and
	calculate state of game.
	
	Options:
	    -v                               Run verbosely
	    -h, --help                       Show this message
	$
	
### Running from command line

change to directory in which **oxs** was cloned or extracted from archive:

	cd o4xs

run **oxs**

	$ ./oxs
	Welcome to TicTacToe board calculator
	Enter TicTacToe board one row at a time,
		each row must consist of three characters.
		allowed characters: x, o, and whitespace.
		
#### Sample run

	$ ./oxs
	Welcome to TicTacToe board calculator
	Enter TicTacToe board one row at a time,
		each row must consist of three characters.
		allowed characters: x, o, and whitespace.
	xox
	xo 
	oxo
	
	Draw
	$ 

#### Getting more information

run **oxs** with a -v command line option to get more information. This option was added in order to provide more information without breaking specification.

	$ ./oxs -v
	Welcome to TicTacToe board calculator
	Enter TicTacToe board one row at a time,
		each row must consist of three characters.
		allowed characters: x, o, and whitespace.
	xxx
	oxo
	xyx
	
	["XXX", "OXO", "XYX"]
	Invalid Input
	Only characters X, O and whitespace are allowed
	$





