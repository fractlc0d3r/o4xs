class Board
	
	def initialize input = ["   ","   ","   "]
		parse input
	end
	
	def parse input
		raise "Expecting array" if input.nil? or !(input.instance_of? Array)
		raise "Must enter three lines" if input.length != 3
		if input.each.map {|l| l.length} != [3,3,3]
			raise "Each line must have 3 characters"
		end
		if input.each.map {|l| l  =~ /[\sXO]{3}/ ? true : false} != [true,true,true]
			raise "Only characters X, O and whitespace are allowed"
		end
		@state = input.each.map {|l| l.each_char.map {|c| Board.encode c}}
	end
	
	# rotate board 90 degrees
	def rotate
		@state.transpose.map &:reverse
	end
	
	def main_diagonal
		0.upto(2).map {|i| @state[i][i]}
	end
	
	def secondary_diagonal
		0.upto(2).map {|i| @state[i][2-i]}
	end
	
	def check_win player
		p = Board.encode player
		case
			# check if any of the rows win
			when ((@state.map {|l| l.inject(0, :+)}).include? p*3)
				true
			# check if any of the columns win
			when ((rotate.map {|l| l.inject(0, :+)}).include? p*3)
				true
			# check if main diagonal wins
			when (main_diagonal.inject(0, :+) == p*3)
				true
			# check if secondary diagonal wins
			when (secondary_diagonal.inject(0, :+) == p*3)
				true
			else
				false
		end
	end
	
	def move_parity
		(@state.map {|l| l.inject(0, :+)}).inject(0, :+)
	end
	
	def check_validity
		# if O had move moves than X 
		# or X had more than one move over O
		# then board is invalid
		if (move_parity != 0) and (move_parity != 1)
			return false
		end
		# both players can't win
		if (check_win('X') and check_win('O'))
			return false
		end
		# if X wins then X made one more move than O
		if check_win('X') and (move_parity != 1)
			return false
		end
		# if Y wins then X and O made equan # of moves
		if check_win('O') and (move_parity != 0)
			return false
		end
		# otherwise consider the board valid	
		true	
	end
	
	def check_draw
		all_rows_have_x = (@state.map{|r| r.include? Board.encode('X')} == [true,true,true])
		all_rows_have_o = (@state.map{|r| r.include? Board.encode('O')} == [true,true,true])
		
		all_cols_have_x = (rotate.map{|r| r.include? Board.encode('X')} == [true,true,true])
		all_cols_have_o = (rotate.map{|r| r.include? Board.encode('O')} == [true,true,true])
		
		main_diag_has_x = (main_diagonal.include? Board.encode('X')) == true
		main_diag_has_o = (main_diagonal.include? Board.encode('O')) == true
		
		sec_diag_has_x = (secondary_diagonal.include? Board.encode('X')) == true
		sec_giad_has_o = (secondary_diagonal.include? Board.encode('O')) == true
		
		# it is a draw if all rows, columns and diaginals have at least one X and O
		all_rows_have_x and all_rows_have_o and all_cols_have_x and all_cols_have_o and \
			main_diag_has_x and main_diag_has_o and sec_diag_has_x and sec_giad_has_o
	end
	
	#private
	def self.encode char
		return case char
			when 'X'
				1
			when 'O'
				-1
			when ' '
				0
		end
	end
	
end